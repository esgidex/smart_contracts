// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "../src/Staking.sol";
import "../src/ESGIDex.sol";
import "../src/LiquidityPoolFactory.sol";

contract StakingTest is Test {
    Staking public staking;
    ESGIDex public esgiDex;
    LiquidityPoolFactory public factory;
    address public owner = address(0x123);
    address public user1 = address(0x456);
    address public user2 = address(0x789);

    function setUp() public {
        vm.deal(owner, 1000 ether);

        // Start the owner context
        vm.startPrank(owner);

        // Deploy the LiquidityPoolFactory contract
        factory = new LiquidityPoolFactory();

        // Deploy the ESGIDex contract with the required arguments
        esgiDex = new ESGIDex(50, owner, address(factory)); // Assuming 50 as swapFee

        // Set the ESGIDex address in the factory contract
        factory.setESGIDex(payable(address(esgiDex)));

        // Deploy the Staking contract with the ESGIDex address
        staking = new Staking(1e18, owner, payable(address(esgiDex))); // 1e18 = 1% reward rate

        // Add funds to the staking contract
        staking.addFunds{value: 1000 ether}();

        // Stop the owner context
        vm.stopPrank();
    }

    function testOwnerAddFunds() public {
        // Check that the contract balance is now 1000 ETH
        assertEq(address(staking).balance, 1000 ether);
    }

    function testStake() public {
        vm.deal(user1, 10 ether);
        vm.prank(user1);
        staking.stake{value: 5 ether}();
        assertEq(staking.getAmountStaked(user1), 5 ether);
        assertEq(staking.getAllEthStaked(), 5 ether);
    }

    function testUnstake() public {
        vm.deal(user1, 10 ether);
        vm.prank(user1);
        staking.stake{value: 5 ether}();

        vm.warp(block.timestamp + 1 days); // Fast forward time by 1 day

        vm.prank(user1);
        staking.unstake(3 ether);
        assertEq(staking.getAmountStaked(user1), 2 ether);
        assertEq(staking.getAllEthStaked(), 2 ether);
    }

    function testClaimRewards() public {
        vm.deal(user1, 10 ether);
        vm.prank(user1);
        staking.stake{value: 1 ether}();

        vm.warp(block.timestamp + 1 days); // Fast forward time by 1 day

        vm.prank(user1);
        staking.claimRewards();

        // Check rewards (1% per day of 1 ether = 0.01 ether)
        uint256 expectedRewards = (1 ether * 1 days * 1e18) / 1e18;
        assertEq(address(user1).balance, expectedRewards);
    }

    function testSetRewardRate() public {
        vm.prank(owner);
        staking.setRewardRate(2e18); // 2% reward rate
        assertEq(staking.rewardRate(), 2e18);
    }

    function testEmergencyWithdraw() public {
        uint256 initialBalance = address(owner).balance;
        vm.prank(owner);
        staking.emergencyWithdraw();
        assertEq(address(staking).balance, 0);
        assertEq(address(owner).balance, initialBalance + 1000 ether);
    }

    function testFailUnstakeMoreThanStaked() public {
        vm.deal(user1, 10 ether);
        vm.prank(user1);
        staking.stake{value: 5 ether}();

        vm.prank(user1);
        staking.unstake(6 ether); // This should fail
    }

    function testFailClaimRewardsWithoutStaking() public {
        vm.prank(user1);
        staking.claimRewards(); // This should fail
    }

    function testPauseAndUnpause() public {
        vm.prank(owner);
        staking.pause();

        vm.deal(user1, 10 ether);
        vm.prank(user1);
        staking.stake{value: 5 ether}();
        vm.expectRevert("Pausable: paused");

        vm.prank(owner);
        staking.unpause();

        vm.prank(user1);
        staking.stake{value: 5 ether}();
        assertEq(staking.getAmountStaked(user1), 5 ether);
    }
}
