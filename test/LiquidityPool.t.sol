// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "../src/LiquidityPool.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TestToken is ERC20 {
    constructor(string memory name, string memory symbol) ERC20(name, symbol) {
        _mint(msg.sender, 10000 * 10 ** 18);
    }
}

contract LiquidityPoolTest is Test {
    LiquidityPool public pool;
    TestToken public tokenA;
    TestToken public tokenB;
    address public owner;
    address public user;

    function setUp() public {
        owner = address(this);
        user = address(1);

        tokenA = new TestToken("Token A", "TKA");
        tokenB = new TestToken("Token B", "TKB");

        pool = new LiquidityPool(address(tokenA), address(tokenB), owner);

        tokenA.transfer(user, 1000 * 10 ** 18);
        tokenB.transfer(user, 1000 * 10 ** 18);

        vm.prank(user);
        tokenA.approve(address(pool), 1000 * 10 ** 18);
        vm.prank(user);
        tokenB.approve(address(pool), 1000 * 10 ** 18);
    }

    function testAddLiquidity() public {
        vm.prank(user);
        pool.addLiquidity(100 * 10 ** 18, 100 * 10 ** 18);

        (uint256 reserveA, uint256 reserveB) = pool.getReserves();
        assertEq(reserveA, 100 * 10 ** 18);
        assertEq(reserveB, 100 * 10 ** 18);
    }

    function testRemoveLiquidity() public {
        vm.prank(user);
        pool.addLiquidity(100 * 10 ** 18, 100 * 10 ** 18);

        vm.prank(user);
        pool.removeLiquidity(50 * 10 ** 18);

        (uint256 reserveA, uint256 reserveB) = pool.getReserves();
        assertEq(reserveA, 50 * 10 ** 18);
        assertEq(reserveB, 50 * 10 ** 18);
    }

    function testDismantlePool() public {
        vm.prank(user);
        pool.addLiquidity(100 * 10 ** 18, 100 * 10 ** 18);

        vm.prank(owner);
        pool.dismantlePool();

        assertEq(tokenA.balanceOf(owner), 100 * 10 ** 18);
        assertEq(tokenB.balanceOf(owner), 100 * 10 ** 18);
    }
}
