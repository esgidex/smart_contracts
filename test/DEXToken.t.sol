// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "../src/DEXToken.sol";

// Contrat de test pour DEXToken
contract DEXTokenTest is Test {
    DEXToken public dexToken;
    address public owner;

    // Configuration initiale avant chaque test
    function setUp() public {
        owner = address(this);
        dexToken = new DEXToken("DEX Token", "DEXT", 18, 10000);
    }

    // Test pour vérifier la création du token et le mint initial
    function testTokenCreation() public {
        assertEq(dexToken.name(), "DEX Token");
        assertEq(dexToken.symbol(), "DEXT");
        assertEq(dexToken.decimals(), 18);
        assertEq(dexToken.totalSupply(), 10000 * 10**18);
        assertEq(dexToken.balanceOf(owner), 10000 * 10**18);
    }

    // Test pour vérifier le transfert de tokens
    function testTransfer() public {
        address recipient = address(0x1);
        uint256 transferAmount = 1000 * 10**18;

        dexToken.transfer(recipient, transferAmount);
        assertEq(dexToken.balanceOf(recipient), transferAmount);
        assertEq(dexToken.balanceOf(owner), (10000 - 1000) * 10**18);
    }

    // Test pour vérifier l'approbation et le transfert depuis
    function testApproveAndTransferFrom() public {
        address spender = address(0x2);
        address recipient = address(0x3);
        uint256 approvalAmount = 500 * 10**18;
        uint256 transferAmount = 300 * 10**18;

        dexToken.approve(spender, approvalAmount);
        assertEq(dexToken.allowance(owner, spender), approvalAmount);

        vm.prank(spender);
        dexToken.transferFrom(owner, recipient, transferAmount);
        assertEq(dexToken.balanceOf(recipient), transferAmount);
        assertEq(dexToken.allowance(owner, spender), approvalAmount - transferAmount);
        assertEq(dexToken.balanceOf(owner), (10000 - 300) * 10**18);
    }
}
