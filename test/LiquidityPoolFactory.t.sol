// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "../src/LiquidityPoolFactory.sol";
import "../src/ESGIDex.sol";
import "../src/LiquidityPool.sol";

contract LiquidityPoolFactoryTest is Test {
    LiquidityPoolFactory factory;
    ESGIDex esgiDex;
    address owner;
    address admin;

    address tokenA = address(0x1);
    address tokenB = address(0x2);
    address priceFeedA = address(0x3);
    address priceFeedB = address(0x4);

    function setUp() public {
        owner = address(this);
        admin = address(0x5);

        factory = new LiquidityPoolFactory();
        
        // Set ESGIDex and admin
        factory.setESGIDex(payable(address(esgiDex)));
    }

    function testFactoryCreation() public {
        assertEq(factory.owner(), owner);
    }

    function testSetESGIDex() public {
        address newESGIDex = address(0x6);
        factory.setESGIDex(payable(newESGIDex));
        assertEq(address(factory.esgiDex()), newESGIDex);
    }

    function testFailSetESGIDexNonOwner() public {
        vm.prank(address(0x7)); // Set msg.sender to 0x7
        factory.setESGIDex(payable(address(0x6)));
    }

    function testCreatePool() public {
        address poolAddress = factory.createPool(tokenA, tokenB, priceFeedA, priceFeedB);
        (address _tokenA, address _tokenB, address _priceFeedA, address _priceFeedB) = factory.getPoolDetails(poolAddress);
        assertEq(_tokenA, tokenA);
        assertEq(_tokenB, tokenB);
        assertEq(_priceFeedA, priceFeedA);
        assertEq(_priceFeedB, priceFeedB);

        address[] memory allPools = factory.getAllPools();
        assertEq(allPools.length, 1);
        assertEq(allPools[0], poolAddress);
    }

    function testFailCreatePoolSameToken() public {
        factory.createPool(tokenA, tokenA, priceFeedA, priceFeedB);
    }

    function testGetPoolAddress() public {
        address poolAddress = factory.createPool(tokenA, tokenB, priceFeedA, priceFeedB);
        address foundPoolAddress = factory.getPoolAddress(tokenA, tokenB);
        assertEq(foundPoolAddress, poolAddress);

        foundPoolAddress = factory.getPoolAddress(tokenB, tokenA);
        assertEq(foundPoolAddress, poolAddress);

        foundPoolAddress = factory.getPoolAddress(address(0x8), address(0x9));
        assertEq(foundPoolAddress, address(0));
    }

    function testRemovePool() public {
        address poolAddress = factory.createPool(tokenA, tokenB, priceFeedA, priceFeedB);

        vm.prank(admin);
        factory.removePool(poolAddress);

        address foundPoolAddress = factory.getPoolAddress(tokenA, tokenB);
        assertEq(foundPoolAddress, address(0));

        address[] memory allPools = factory.getAllPools();
        assertEq(allPools.length, 0);
    }

    function testFailRemovePoolNonAdmin() public {
        address poolAddress = factory.createPool(tokenA, tokenB, priceFeedA, priceFeedB);
        factory.removePool(poolAddress);
    }
}
