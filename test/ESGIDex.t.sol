// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import "../src/ESGIDex.sol";
import "../src/LiquidityPoolFactory.sol";
import "../src/LiquidityPool.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TestToken is ERC20 {
    constructor(string memory name, string memory symbol) ERC20(name, symbol) {
        _mint(msg.sender, 10000 * 10 ** 18);
    }
}

contract MockAggregator is AggregatorV3Interface {
    int256 private _price;
    uint8 private _decimals;

    constructor(int256 price_, uint8 decimals_) {
        _price = price_;
        _decimals = decimals_;
    }

    function decimals() external view override returns (uint8) {
        return _decimals;
    }

    function description() external view override returns (string memory) {
        return "MockAggregator";
    }

    function version() external view override returns (uint256) {
        return 1;
    }

    function latestRoundData()
        external
        view
        override
        returns (uint80, int256 answer, uint256, uint256, uint80)
    {
        return (0, _price, 0, 0, 0);
    }

    function getRoundData(
        uint80
    )
        external
        view
        override
        returns (uint80, int256, uint256, uint256, uint80)
    {
        return (0, _price, 0, 0, 0);
    }
}

contract ESGIDexTest is Test {
    ESGIDex public dex;
    LiquidityPoolFactory public factory;
    LiquidityPool public pool;
    TestToken public tokenA;
    TestToken public tokenB;
    MockAggregator public aggregatorA;
    address public owner;

    function setUp() public {
        tokenA = new TestToken("Token A", "TKA");
        tokenB = new TestToken("Token B", "TKB");
        aggregatorA = new MockAggregator(2000 * 10 ** 8, 8); // 2000 USD with 8 decimals
        owner = address(this);

        factory = new LiquidityPoolFactory();
        factory.createPool(
            address(tokenA),
            address(tokenB),
            address(aggregatorA),
            address(0)
        );

        address poolAddress = factory.getPoolAddress(address(tokenA), address(tokenB));
        pool = LiquidityPool(poolAddress);

        // dex = new ESGIDex(50, address(0), address(factory), owner); // 0.5% fee
    }

    function testSwap() public {
        tokenA.approve(address(dex), 1000 * 10 ** 18);
        tokenB.transfer(address(pool), 1000 * 10 ** 18);
        tokenA.transfer(address(pool), 1000 * 10 ** 18);

        uint256 amountOut = dex.swap(
            address(tokenA),
            address(tokenB),
            100 * 10 ** 18
        );

        uint256 fee = (100 * 10 ** 18 * 50) / 10000;
        uint256 amountInAfterFee = 100 * 10 ** 18 - fee;
        uint256 expectedAmountOut = (amountInAfterFee * 1000 * 10 ** 18) /
            (1000 * 10 ** 18 + amountInAfterFee);
        assertEq(amountOut, expectedAmountOut);
    }

    function testClaimFees() public {
        tokenA.approve(address(dex), 1000 * 10 ** 18);
        tokenB.transfer(address(pool), 1000 * 10 ** 18);
        tokenA.transfer(address(pool), 1000 * 10 ** 18);

        dex.swap(address(tokenA), address(tokenB), 100 * 10 ** 18);

        uint256 feesBefore = address(this).balance;
        dex.claimFees();
        uint256 feesAfter = address(this).balance;

        assertGt(feesAfter, feesBefore);
    }

    function testSetSwapFee() public {
        uint256 newFee = 100; // 1%
        dex.setSwapFee(newFee);
        assertEq(dex.swapFee(), newFee);
    }

    function testRevertSwapWithSameToken() public {
        tokenA.approve(address(dex), 100 * 10 ** 18);
        vm.expectRevert("Cannot swap the same token");
        dex.swap(address(tokenA), address(tokenA), 100 * 10 ** 18);
    }

    function testRevertSwapWithZeroAmount() public {
        tokenA.approve(address(dex), 100 * 10 ** 18);
        vm.expectRevert("Amount must be greater than zero");
        dex.swap(address(tokenA), address(tokenB), 0);
    }

    function testRevertClaimFeesWithNoFees() public {
        vm.expectRevert("No fees to claim");
        dex.claimFees();
    }
}
