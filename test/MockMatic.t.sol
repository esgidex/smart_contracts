// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import {MockMATIC} from "../src/MockMatic.sol";

contract MockMATICTest is Test {
    MockMATIC mockMatic;

    function setUp() public {
        // Déploiement du contrat MockMATIC
        mockMatic = new MockMATIC();
    }

    function testInitialBalance() public {
        // Vérification que le solde initial du déployeur est de 1 000 000 mMATIC
        uint256 expectedBalance = 1000000 * (10 ** mockMatic.decimals());
        assertEq(mockMatic.balanceOf(address(this)), expectedBalance);
    }

    function testTransfer() public {
        // Adresse de destination pour le transfert
        address recipient = address(0x123);
        uint256 amount = 1000 * (10 ** mockMatic.decimals());

        // Transfert de 1000 mMATIC au destinataire
        mockMatic.transfer(recipient, amount);

        // Vérification du solde du destinataire
        assertEq(mockMatic.balanceOf(recipient), amount);

        // Vérification de la diminution du solde de l'émetteur
        uint256 expectedBalance = 999000 * (10 ** mockMatic.decimals());
        assertEq(mockMatic.balanceOf(address(this)), expectedBalance);
    }
}
