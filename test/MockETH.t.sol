// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Test.sol";
import {MockETH} from "../src/MockETH.sol";

contract MockETHTest is Test {
    MockETH mockEth;

    function setUp() public {
        // Déploiement du contrat MockETH
        mockEth = new MockETH();
    }

    function testInitialBalance() public {
        // Vérification que le solde initial du déployeur est de 1 000 000 mETH
        uint256 expectedBalance = 1000000 * (10 ** mockEth.decimals());
        assertEq(mockEth.balanceOf(address(this)), expectedBalance);
    }

    function testTransfer() public {
        // Adresse de destination pour le transfert
        address recipient = address(0x123);
        uint256 amount = 1000 * (10 ** mockEth.decimals());

        // Transfert de 1000 mETH au destinataire
        mockEth.transfer(recipient, amount);

        // Vérification du solde du destinataire
        assertEq(mockEth.balanceOf(recipient), amount);

        // Vérification de la diminution du solde de l'émetteur
        uint256 expectedBalance = 999000 * (10 ** mockEth.decimals());
        assertEq(mockEth.balanceOf(address(this)), expectedBalance);
    }
}
