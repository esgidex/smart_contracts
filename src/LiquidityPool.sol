// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";

contract LiquidityPool is ReentrancyGuard, Ownable {
    address public tokenA;
    address public tokenB;
    address public esgiDex;
    uint256 public totalLiquidity;
    mapping(address => uint256) public liquidity;
    mapping(address => uint256) public depositsTokenA;
    mapping(address => uint256) public depositsTokenB;
    mapping(address => uint256) public collectedFees;
    mapping(address => uint256) public userRewards;
    mapping(address => uint256) public feeInDifferentToken;
    uint256 public totalRewardsDistributed;
    uint256 public poolCreationTime;
    address[] private liquidityProviders;

    event LiquidityAdded(
        address indexed provider,
        uint256 amountA,
        uint256 amountB
    );
    event LiquidityRemoved(
        address indexed provider,
        uint256 amountA,
        uint256 amountB
    );
    event TokensSwapped(
        address indexed swapper,
        address tokenIn,
        uint256 amountIn,
        address tokenOut,
        uint256 amountOut,
        uint256 fee
    );
    event RewardsClaimed(address indexed user, uint256 reward);
    event RewardDistributed(address indexed user, uint256 reward); // Log for rewards distributed
    event DebugLog(string message, uint256 value); // Debug log

    constructor(
        address _tokenA,
        address _tokenB,
        address _owner,
        address _esgiDex
    ) Ownable(_owner) {
        tokenA = _tokenA;
        tokenB = _tokenB;
        esgiDex = _esgiDex;
        poolCreationTime = block.timestamp;
        emit DebugLog("Pool created", block.timestamp);
    }

    function receiveFeeFromDex(address token, uint256 amount) public {
        feeInDifferentToken[token] += amount;
    }

    function getFeeBalance(address token) public view returns (uint256) {
        return feeInDifferentToken[token];
    }

    function addLiquidity(
        uint256 amountA,
        uint256 amountB
    ) external nonReentrant {
        emit DebugLog("addLiquidity - amountA", amountA);
        emit DebugLog("addLiquidity - amountB", amountB);

        IERC20(tokenA).approve(esgiDex, amountA);
        IERC20(tokenB).approve(esgiDex, amountB);
        IERC20(tokenA).transferFrom(msg.sender, address(this), amountA);
        IERC20(tokenB).transferFrom(msg.sender, address(this), amountB);

        liquidity[msg.sender] += amountA + amountB;
        totalLiquidity += amountA + amountB;

        depositsTokenA[msg.sender] += amountA;
        depositsTokenB[msg.sender] += amountB;

        if (liquidity[msg.sender] == amountA + amountB) {
            liquidityProviders.push(msg.sender);
        }

        emit LiquidityAdded(msg.sender, amountA, amountB);
    }

    function removeLiquidity() external nonReentrant {
        uint256 userLiquidity = liquidity[msg.sender];
        require(userLiquidity > 0, "No liquidity to remove");

        uint256 amountA = depositsTokenA[msg.sender];
        uint256 amountB = depositsTokenB[msg.sender];

        emit DebugLog("removeLiquidity - amountA", amountA);
        emit DebugLog("removeLiquidity - amountB", amountB);

        liquidity[msg.sender] = 0;
        totalLiquidity -= userLiquidity;

        depositsTokenA[msg.sender] = 0;
        depositsTokenB[msg.sender] = 0;

        IERC20(tokenA).transfer(msg.sender, amountA);
        IERC20(tokenB).transfer(msg.sender, amountB);

        removeLiquidityProvider(msg.sender);

        emit LiquidityRemoved(msg.sender, amountA, amountB);
    }

    function distributeSwapFees(uint256 fee) external onlyOwner {
        emit DebugLog("distributeSwapFees - fee", fee);

        if (fee > 0) {
            for (uint256 i = 0; i < liquidityProviders.length; i++) {
                address provider = liquidityProviders[i];
                uint256 providerLiquidity = liquidity[provider];
                if (providerLiquidity > 0) {
                    uint256 reward = (fee * providerLiquidity) / totalLiquidity;
                    userRewards[provider] += reward;
                    emit RewardDistributed(provider, reward);
                }
            }
            totalRewardsDistributed += fee;
        }
    }

    function getReserves()
        external
        view
        returns (uint256 reserveA, uint256 reserveB)
    {
        reserveA = IERC20(tokenA).balanceOf(address(this));
        reserveB = IERC20(tokenB).balanceOf(address(this));
    }

    function getLiquidity(address user) external view returns (uint256) {
        return liquidity[user];
    }

    function getCollectedFees(address token) external view returns (uint256) {
        return collectedFees[token];
    }

    function claimRewards() external nonReentrant {
        uint256 reward = userRewards[msg.sender];
        require(reward > 0, "No rewards to claim");
        emit DebugLog("claimRewards - reward", reward);

        userRewards[msg.sender] = 0;
        IERC20(tokenA).transfer(msg.sender, reward);

        emit RewardsClaimed(msg.sender, reward);
    }

    function getUserRewards(address user) external view returns (uint256) {
        return userRewards[user];
    }

    function calculateAPY() external view returns (uint256) {
        uint256 timeElapsed = block.timestamp - poolCreationTime;
        uint256 daysElapsed = timeElapsed / 1 days;
        if (daysElapsed == 0) {
            return 0;
        }
        uint256 apy = (totalRewardsDistributed * 365 * 100) /
            (totalLiquidity * daysElapsed);
        return apy;
    }

    function dismantlePool() external nonReentrant {
        uint256 reserveA = IERC20(tokenA).balanceOf(address(this));
        uint256 reserveB = IERC20(tokenB).balanceOf(address(this));
        uint256 feeA = collectedFees[tokenA];
        uint256 feeB = collectedFees[tokenB];

        emit DebugLog("dismantlePool - reserveA", reserveA);
        emit DebugLog("dismantlePool - reserveB", reserveB);

        if (feeA > 0) {
            IERC20(tokenA).transfer(owner(), feeA);
        }
        if (feeB > 0) {
            IERC20(tokenB).transfer(owner(), feeB);
        }

        for (uint256 i = 0; i < liquidityProviders.length; i++) {
            address provider = liquidityProviders[i];
            uint256 amountA = depositsTokenA[provider];
            uint256 amountB = depositsTokenB[provider];

            if (amountA > 0 || amountB > 0) {
                depositsTokenA[provider] = 0;
                depositsTokenB[provider] = 0;
                IERC20(tokenA).transfer(provider, amountA);
                IERC20(tokenB).transfer(provider, amountB);
            }
        }

        selfdestruct(payable(owner()));
    }

    function removeLiquidityProvider(address provider) private {
        uint256 length = liquidityProviders.length;
        for (uint256 i = 0; i < length; i++) {
            if (liquidityProviders[i] == provider) {
                liquidityProviders[i] = liquidityProviders[length - 1];
                liquidityProviders.pop();
                break;
            }
        }
    }
}
