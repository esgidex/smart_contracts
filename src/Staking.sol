// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import "@openzeppelin/contracts/utils/Pausable.sol";
import "./ESGIDex.sol";

contract Staking is Ownable, ReentrancyGuard, Pausable {
    uint256 public rewardRate;
    uint256 public totalStaked;
    address[] private stakers;
    ESGIDex public esgiDex; // Address of the ESGIDex contract
    mapping(address => uint256) public amountStaked;
    mapping(address => bool) private hasStaked;

    struct Stake {
        uint256 amount;
        uint256 rewardDebt;
        uint256 lastRewardTime;
    }

    mapping(address => Stake) public stakes;

    event Staked(address indexed user, uint256 amount);
    event Unstaked(address indexed user, uint256 amount);
    event RewardsClaimed(address indexed user, uint256 amount);
    event RewardRateUpdated(uint256 newRewardRate);
    event EmergencyWithdraw(address indexed owner, uint256 amount);
    event FundsAdded(address indexed admin, uint256 amount);

    constructor(
        uint256 _rewardRate,
        address _owner,
        address payable _esgiDex
    ) Ownable(_owner) {
        rewardRate = _rewardRate;
        require(_esgiDex != address(0), "ESGIDex address cannot be zero");
        esgiDex = ESGIDex(_esgiDex);
    }

    function stake() external payable nonReentrant whenNotPaused {
        require(msg.value > 0, "Cannot stake 0 ETH");

        Stake storage userStake = stakes[msg.sender];
        updateRewards(msg.sender);

        userStake.amount += msg.value;
        userStake.lastRewardTime = block.timestamp;

        totalStaked += msg.value;
        amountStaked[msg.sender] += msg.value;

        if (!hasStaked[msg.sender]) {
            stakers.push(msg.sender);
            hasStaked[msg.sender] = true;
        }
    }

    function unstake(uint256 amount) external nonReentrant whenNotPaused {
        Stake storage userStake = stakes[msg.sender];
        require(userStake.amount >= amount, "Not enough staked ETH");

        updateRewards(msg.sender);

        userStake.amount -= amount;
        payable(msg.sender).transfer(amount);

        totalStaked -= amount;
        amountStaked[msg.sender] -= amount;

        if (amountStaked[msg.sender] == 0) {
            hasStaked[msg.sender] = false;
        }
    }

    function getStakerCount() public view returns (uint256) {
        return stakers.length;
    }

    function claimRewards() external nonReentrant whenNotPaused {
        updateRewards(msg.sender);

        Stake storage userStake = stakes[msg.sender];
        uint256 rewards = userStake.rewardDebt;

        require(rewards > 0, "No rewards available");
        require(
            address(this).balance >= rewards,
            "Insufficient contract balance"
        );

        userStake.rewardDebt = 0;
        payable(msg.sender).transfer(rewards);
    }

    function updateRewards(address account) internal {
        Stake storage userStake = stakes[account];
        if (userStake.amount > 0) {
            uint256 stakingDuration = block.timestamp -
                userStake.lastRewardTime;
            uint256 pendingRewards = (userStake.amount *
                stakingDuration *
                rewardRate) / 1e18;
            userStake.rewardDebt += pendingRewards;
        }
        userStake.lastRewardTime = block.timestamp;
    }

    function getAmountStaked(address account) external view returns (uint256) {
        return amountStaked[account];
    }

    function getAllEthStaked() external view returns (uint256) {
        return totalStaked;
    }

    function setRewardRate(uint256 _rewardRate) external {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        rewardRate = _rewardRate;
    }

    function emergencyWithdraw() external nonReentrant {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        payable(owner()).transfer(address(this).balance);
    }

    function addFunds() external payable {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        require(msg.value > 0, "Cannot add 0 ETH");
        emit FundsAdded(msg.sender, msg.value);
    }

    function pause() external {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        _pause();
    }

    function unpause() external {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        _unpause();
    }
    receive() external payable {
        totalStaked += msg.value;
    }

    fallback() external payable {
        totalStaked += msg.value;
    }
}
