// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "./LiquidityPool.sol";
import "./ESGIDex.sol";

contract LiquidityPoolFactory {
    address public owner; // Address of the contract owner
    ESGIDex public esgiDex; // Address of the ESGIDex contract
    address[] public allPools; // List of all created pools
    address[] public allTokens; // List of all tokens
    mapping(address => Pool) public pools; // Mapping to store details of each pool

    struct Pool {
        // Structure to store details of a pool
        address tokenA;
        address tokenB;
        address poolAddress;
        address priceFeedA;
        address priceFeedB;
    }

    // Events to track important actions
    event PoolCreated(
        address indexed poolAddress,
        address tokenA,
        address tokenB
    );

    event DebugLog(string message, uint256 value); // Debug log

    constructor() {
        owner = msg.sender;
        emit DebugLog("Factory created", block.timestamp);
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner can call this function");
        _;
    }

    // Function to set the address of ESGIDex
    function setESGIDex(address payable _esgiDex) external onlyOwner {
        require(_esgiDex != address(0), "ESGIDex address cannot be zero");
        esgiDex = ESGIDex(_esgiDex);
    }

    // Helper function to check if a token is already in the allTokens array
    function isTokenInAllTokens(address token) internal view returns (bool) {
        for (uint256 i = 0; i < allTokens.length; i++) {
            if (allTokens[i] == token) {
                return true;
            }
        }
        return false;
    }

    // Function to create a new liquidity pool
    function createPool(
        address tokenA,
        address tokenB,
        address priceFeedA,
        address priceFeedB
    ) external returns (address) {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        require(tokenA != address(0), "Token A address cannot be zero");
        require(tokenB != address(0), "Token B address cannot be zero");
        require(tokenA != tokenB, "Token A and Token B must be different");

        LiquidityPool newPool = new LiquidityPool(
            tokenA,
            tokenB,
            owner,
            address(esgiDex)
        );
        address poolAddress = address(newPool);
        allPools.push(poolAddress);

        pools[poolAddress] = Pool({
            tokenA: tokenA,
            tokenB: tokenB,
            poolAddress: poolAddress,
            priceFeedA: priceFeedA,
            priceFeedB: priceFeedB
        });

        if (!isTokenInAllTokens(tokenA)) {
            allTokens.push(tokenA);
        }

        if (!isTokenInAllTokens(tokenB)) {
            allTokens.push(tokenB);
        }

        emit PoolCreated(poolAddress, tokenA, tokenB);
        emit DebugLog(
            "Pool created - poolAddress",
            uint256(uint160(poolAddress))
        );

        return poolAddress;
    }

    // Function to get the address of the pool for two tokens
    function getPoolAddress(
        address tokenA,
        address tokenB
    ) external view returns (address) {
        for (uint i = 0; i < allPools.length; i++) {
            Pool memory pool = pools[allPools[i]];
            if (
                (pool.tokenA == tokenA && pool.tokenB == tokenB) ||
                (pool.tokenA == tokenB && pool.tokenB == tokenA)
            ) {
                return pool.poolAddress;
            }
        }
        return address(0);
    }

    // Function to get the list of all pools
    function getAllPools() external view returns (address[] memory) {
        return allPools;
    }

    // Function to get the list of all tokens
    function getAllTokens() external view returns (address[] memory) {
        return allTokens;
    }

    // Function to get the details of a specific pool
    function getPoolDetails(
        address pool
    ) external view returns (address, address, address, address) {
        Pool storage liquidityPool = pools[pool];
        return (
            liquidityPool.tokenA,
            liquidityPool.tokenB,
            liquidityPool.priceFeedA,
            liquidityPool.priceFeedB
        );
    }

    // Function to remove a pool from the list and dismantle it
    function removePool(address _pool) external {
        require(
            esgiDex.isAdmin(msg.sender),
            "Only admin can call this function"
        );
        LiquidityPool liquidityPool = LiquidityPool(_pool);
        address tokenA = liquidityPool.tokenA();
        address tokenB = liquidityPool.tokenB();
        liquidityPool.dismantlePool();
        // Remove the pool from the list
        for (uint256 i = 0; i < allPools.length; i++) {
            if (allPools[i] == _pool) {
                allPools[i] = allPools[allPools.length - 1];
                allPools.pop();
                break;
            }
        }

        // Remove token from allTokens if no other pool uses it
        bool tokenAUsed = false;
        bool tokenBUsed = false;
        for (uint256 i = 0; i < allPools.length; i++) {
            Pool memory pool = pools[allPools[i]];
            if (pool.tokenA == tokenA || pool.tokenB == tokenA) {
                tokenAUsed = true;
            }
            if (pool.tokenA == tokenB || pool.tokenB == tokenB) {
                tokenBUsed = true;
            }
        }

        if (!tokenAUsed) {
            for (uint256 i = 0; i < allTokens.length; i++) {
                if (allTokens[i] == tokenA) {
                    allTokens[i] = allTokens[allTokens.length - 1];
                    allTokens.pop();
                    break;
                }
            }
        }

        if (!tokenBUsed) {
            for (uint256 i = 0; i < allTokens.length; i++) {
                if (allTokens[i] == tokenB) {
                    allTokens[i] = allTokens[allTokens.length - 1];
                    allTokens.pop();
                    break;
                }
            }
        }
    }
}
