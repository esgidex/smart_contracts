// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import "./LiquidityPoolFactory.sol";
import "./LiquidityPool.sol"; // Importation ajoutée

contract ESGIDex is Ownable, ReentrancyGuard {
    uint256 public swapFee; // Fee in basis points (1 basis point = 0.01%)
    LiquidityPoolFactory public factory;
    address public feeCollector;

    struct Admin {
        bool exists;
        uint256 index;
    }
    mapping(address => Admin) private adminsMapping;
    mapping(address => uint256) public dexFees;
    address[] private adminList;

    struct BannedUser {
        bool exists;
        uint256 index;
    }
    mapping(address => BannedUser) private bannedUsersMapping;
    address[] private bannedUserList;

    event TokensSwapped(
        address indexed swapper,
        address tokenIn,
        uint256 amountIn,
        address tokenOut,
        uint256 amountOut
    );
    event FeesClaimed(address indexed owner, uint256 amount);
    event SwapFeeSet(uint256 newSwapFee);
    event UserBanned(address indexed user);
    event UserUnbanned(address indexed user);
    event AdminAdded(address indexed admin);
    event AdminRemoved(address indexed admin);

    modifier onlyAdmin() {
        require(
            adminsMapping[msg.sender].exists,
            "Only admin can call this function"
        );
        _;
    }

    modifier notBanned() {
        require(!bannedUsersMapping[msg.sender].exists, "User is banned");
        _;
    }

    constructor(
        uint256 _swapFee,
        address _owner,
        address _factoryAddr
    ) Ownable(_owner) {
        require(
            _swapFee <= 10000,
            "Fee percentage must be between 0 and 10000 basis points"
        );
        swapFee = _swapFee;
        adminsMapping[msg.sender] = Admin(true, adminList.length);
        adminList.push(msg.sender);
        require(_factoryAddr != address(0), "Factory address cannot be zero");
        factory = LiquidityPoolFactory(_factoryAddr);
    }

    function getSwapFee() external view returns (uint256) {
        return swapFee;
    }

    function estimateSwap(
        address tokenIn,
        address tokenOut,
        uint256 amountIn
    ) public view returns (uint256 amountOut, uint256 fee) {
        require(tokenIn != tokenOut, "Cannot swap the same token");
        require(amountIn > 0, "Amount must be greater than zero");

        address poolAddress = factory.getPoolAddress(tokenIn, tokenOut);
        require(
            poolAddress != address(0),
            "No liquidity pool available for these tokens"
        );

        uint256 reserveIn = IERC20(tokenIn).balanceOf(poolAddress);
        uint256 reserveOut = IERC20(tokenOut).balanceOf(poolAddress);
        require(reserveOut > 0, "Insufficient liquidity for this trade");

        fee = (amountIn * swapFee) / 10000;
        uint256 amountInAfterFee = amountIn - fee;
        amountOut =
            (amountInAfterFee * reserveOut) /
            (reserveIn + amountInAfterFee);
    }

    function swap(
        address tokenIn,
        address tokenOut,
        uint256 amountIn
    ) external payable nonReentrant notBanned returns (uint256 amountOut) {
        require(tokenIn != address(0), "TokenIn address cannot be zero");
        require(tokenOut != address(0), "TokenOut address cannot be zero");
        require(tokenIn != tokenOut, "Cannot swap the same token");
        require(amountIn > 0, "Amount must be greater than zero");

        // Retrieve the address of the liquidity pool
        address poolAddress = factory.getPoolAddress(tokenIn, tokenOut);
        require(
            poolAddress != address(0),
            "No liquidity pool available for these tokens"
        );

        // Retrieve the reserves of the liquidity pool
        uint256 reserveIn = IERC20(tokenIn).balanceOf(poolAddress);
        uint256 reserveOut = IERC20(tokenOut).balanceOf(poolAddress);
        require(reserveOut > 0, "Insufficient liquidity for this trade");

        // Calculate fee
        uint256 fee = (amountIn * swapFee) / 10000;
        uint256 amountInAfterFee = amountIn - fee;
        amountOut =
            (amountInAfterFee * reserveOut) /
            (reserveIn + amountInAfterFee);

        require(amountOut <= reserveOut, "Not enough liquidity for this swap");

        // Check allowances
        checkAllowances(
            tokenIn,
            tokenOut,
            poolAddress,
            amountInAfterFee,
            amountOut
        );

        // Transfer tokens
        IERC20(tokenIn).transferFrom(msg.sender, address(this), amountIn);
        IERC20(tokenIn).transfer(poolAddress, amountInAfterFee);
        IERC20(tokenOut).transferFrom(poolAddress, msg.sender, amountOut);

        emit TokensSwapped(msg.sender, tokenIn, amountIn, tokenOut, amountOut);
    }

    function checkAllowances(
        address tokenIn,
        address tokenOut,
        address poolAddress,
        uint256 amountInAfterFee,
        uint256 amountOut
    ) internal view {
        uint256 allowance = IERC20(tokenIn).allowance(
            msg.sender,
            address(this)
        );
        require(allowance >= amountInAfterFee, "Check the token allowance");

        uint256 poolAllowance = IERC20(tokenOut).allowance(
            msg.sender,
            poolAddress
        );
        require(poolAllowance >= amountOut, "Check the pool token allowance");
    }

    function setSwapFee(uint256 _swapFee) external onlyAdmin {
        require(
            _swapFee <= 10000,
            "Fee percentage must be between 0 and 10000 basis points"
        );
        swapFee = _swapFee;
        emit SwapFeeSet(_swapFee);
    }

    function claimFees() external onlyAdmin {
        // admins can claim fees to (relayer) owner
        uint256 feeAmount = address(this).balance;
        require(feeAmount > 0, "No fees to claim");

        payable(owner()).transfer(feeAmount);

        emit FeesClaimed(owner(), feeAmount);
    }

    function getBalance() external view returns (uint256) {
        return address(this).balance;
    }

    // Nouvelle fonction pour obtenir les frais collectés par token pour tous les pools
    function getAllCollectedFees(
        address token
    ) external view returns (uint256 totalFees) {
        address[] memory pools = factory.getAllPools();
        for (uint256 i = 0; i < pools.length; i++) {
            totalFees += LiquidityPool(pools[i]).getCollectedFees(token);
        }
    }

    // Nouvelle fonction pour réclamer les récompenses de l'utilisateur
    function claimUserRewards() external nonReentrant {
        address[] memory pools = factory.getAllPools();
        for (uint256 i = 0; i < pools.length; i++) {
            LiquidityPool(pools[i]).claimRewards();
        }
    }

    // Nouvelle fonction pour obtenir les récompenses d'un utilisateur
    function getUserRewards(
        address user
    ) external view returns (uint256 totalRewards) {
        address[] memory pools = factory.getAllPools();
        for (uint256 i = 0; i < pools.length; i++) {
            totalRewards += LiquidityPool(pools[i]).getUserRewards(user);
        }
    }

    // Function to ban a user
    function banUser(address user) external onlyAdmin {
        require(isAdmin(msg.sender), "Only admin can call this function");
        require(!bannedUsersMapping[user].exists, "User already banned");
        bannedUsersMapping[user] = BannedUser(true, bannedUserList.length);
        bannedUserList.push(user);
        emit UserBanned(user);
    }

    // Function to unban a user
    function unbanUser(address user) external onlyAdmin {
        require(isAdmin(msg.sender), "Only admin can call this function");
        require(bannedUsersMapping[user].exists, "User not banned");
        uint256 index = bannedUsersMapping[user].index;
        uint256 lastIndex = bannedUserList.length - 1;
        address lastUser = bannedUserList[lastIndex];

        bannedUserList[index] = lastUser;
        bannedUsersMapping[lastUser].index = index;

        bannedUserList.pop();
        delete bannedUsersMapping[user];

        emit UserUnbanned(user);
    }

    // Function to add an administrator
    function addAdmin(address admin) external onlyAdmin {
        require(!adminsMapping[admin].exists, "Admin already exists");
        adminsMapping[admin] = Admin(true, adminList.length);
        adminList.push(admin);
        emit AdminAdded(admin);
    }

    // Function to remove an administrator
    function removeAdmin(address admin) external onlyAdmin {
        require(adminsMapping[admin].exists, "Admin does not exist");
        uint256 index = adminsMapping[admin].index;
        uint256 lastIndex = adminList.length - 1;
        address lastAdmin = adminList[lastIndex];

        adminList[index] = lastAdmin;
        adminsMapping[lastAdmin].index = index;

        adminList.pop();
        delete adminsMapping[admin];

        emit AdminRemoved(admin);
    }

    function getAdmins() external view returns (address[] memory) {
        return adminList;
    }

    function getBannedUsers() external view returns (address[] memory) {
        return bannedUserList;
    }

    function isAdmin(address account) public view returns (bool) {
        return adminsMapping[account].exists;
    }

    receive() external payable {}
}
