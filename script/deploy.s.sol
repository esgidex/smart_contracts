// SPDX-License-Identifier: MIT
pragma solidity ^0.8.20;

import "forge-std/Script.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../src/LiquidityPoolFactory.sol";
import "../src/ESGIDex.sol";

contract DeployContracts is Script {
    LiquidityPoolFactory public liquidityPoolFactory;
    ESGIDex public esgiDex;
    address public linkToken = 0x779877A7B0D9E8603169DdbD7836e478b4624789;
    address public daiToken = 0xFF34B3d4Aee8ddCd6F9AFFFB6Fe49bD371b8a357;
    address public linkUsdPriceFeed =
        0xc59E3633BAAC79493d908e63626716e204A45EdF;
    address public daiUsdPriceFeed = 0x14866185B1962B63C3Ea9E03Bc1da838bab34C19;

    event ContractDeployed(string name, address addr);

    function run() external {
        uint256 deployerPrivateKey = vm.envUint("PKEY");
        vm.startBroadcast(deployerPrivateKey);

        // Déployer le contract LiquidityPoolFactory
        liquidityPoolFactory = new LiquidityPoolFactory();
        emit ContractDeployed(
            "LiquidityPoolFactory",
            address(liquidityPoolFactory)
        );

        // Déployer le contract ESGIDex avec le CA de la factory
        esgiDex = new ESGIDex(30, msg.sender, address(liquidityPoolFactory));
        emit ContractDeployed("ESGIDex", address(esgiDex));

        // Jouer la fonction setESGIDex de la factory avec le CA d’ESGIDex en argument
        liquidityPoolFactory.setESGIDex(payable(address(esgiDex)));

        // Créer une pool depuis sur LiquidityPoolFactory
        address poolAddress = liquidityPoolFactory.createPool(
            linkToken,
            daiToken,
            linkUsdPriceFeed,
            daiUsdPriceFeed
        );
        emit ContractDeployed("LiquidityPool", poolAddress);

        // Approuver les tokens pour la pool
        IERC20(linkToken).approve(poolAddress, type(uint256).max);
        IERC20(daiToken).approve(poolAddress, type(uint256).max);

        vm.stopBroadcast();
    }
}
