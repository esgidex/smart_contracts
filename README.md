# Dex Smart Contracts

This repository contains Solidity smart contracts for a DEX platform using Ethereum (ETH). The smart contracts allow users to stake ETH, earn rewards based on the amount staked and the reward rate, and withdraw their staked ETH along with earned rewards also users will be able to add liquidity in LPs, Swap tokens.

## Table of Contents

- [Overview](#overview)
- [Smart Contracts](#smart-contracts)
- [Setup](#setup)
- [Testing](#testing)
- [Deployment](#deployment)
- [Contributing](#contributing)
- [License](#license)

## Overview

The Staking smart contract allows users to:
- Stake ETH into the contract.
- Earn rewards over time based on a predefined reward rate.
- Claim accumulated rewards.
- Unstake their ETH partially or completely.
- The contract owner can also perform an emergency withdrawal of all ETH in the contract.

## Smart Contracts

### Staking.sol

The main contract for staking ETH. Key functionalities include:
- `stake()`: Stake ETH into the contract.
- `unstake(uint256 amount)`: Unstake a specified amount of ETH.
- `claimRewards()`: Claim accumulated staking rewards.
- `setRewardRate(uint256 _rewardRate)`: Set the reward rate (only owner).
- `emergencyWithdraw()`: Withdraw all ETH from the contract (only owner).

### DEXToken.sol

This contract create the plateform's token. It is a ERC-20 standard.

### LiquidityPool.sol

The contract for liquidity pool. Key functionalities include : 
- `addLiquidity()` : Add liquidity.
- `removeLiquidity()` : Remove liquidity.
- `swap()` : Swap a token A to get Token B in return.
- `claimFees()` : Claim swap's fees, only for the owner of the contract.
- `getReserves()` : Get the LP's reserve.

### LiquidityPoolFactory.sol 

The contract to generate liquiditypool. Key functionalities include : 

- `createPool()` : Create pool with 2 tokens.
- `getAllPools()` : Returns all pools.
- `getPoolDetails()` : Returns details of one pool.


## Setup

To set up the development environment, you need to have the following installed:

- [Foundry](https://github.com/gakonst/foundry)
- [Solidity](https://soliditylang.org/)

### Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/esgidex/smart_contracts.git
    cd smart_contracts
    ```

2. Install dependencies:

    ```bash
    forge install
    ```

3. Compile the contracts:

    ```bash
    forge build
    ```

## Testing

The repository uses Foundry for testing. The tests ensure that all functionalities of the Staking contract work as expected.

### Running Tests

To run the tests, execute the following command:

    ```bash
    forge test -vvvv
    ```

This will run all the tests in the `test` directory and provide verbose output.

## Deployment

To deploy the Staking contract, you can use any Ethereum development framework such as Hardhat, Truffle, or Remix. Below is an example of deploying using Hardhat.

## Contributing

Contributions are welcome! Please follow these steps to contribute:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Commit your changes (`git commit -m 'Add new feature'`).
4. Push to the branch (`git push origin feature-branch`).
5. Create a pull request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
